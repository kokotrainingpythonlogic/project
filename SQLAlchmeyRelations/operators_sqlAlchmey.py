# from pip._vendor.urllib3 import connection
# from sqlalchemy import Integer, String, select, engine
# from sqlalchemy.orm import Session, sessionmaker
# from sqlalchemy_wrapper import SQLAlchemy
# from sqlalchemy.sql import text
#
#
# db = SQLAlchemy('sqlite:///operator1.db')
#
#
# class Student(db.Model):
#     __tablename__ = 'student1'
#
#     id = db.Column(Integer, primary_key=True)
#     name = db.Column(String)
#     address = db.Column(String)
#     phone = db.Column(String)
#     course = db.Column(String)
#
#
# db.create_all()
# Session=sessionmaker(bind=db)
# session=Session()
# # s1 = Student(name="Akshay", address="Solapur", phone="8149004849", course="C++")
# # db.add(s1)
# # s2 = Student(name="Piyush", address="Pune", phone="123456789", course='Python')
# # db.add(s2)
# # s3 = Student(name="Amit", address="Mumbai", phone="123456789", course='Python')
# # db.add(s3)
# # s4 = Student(name="Ankita", address="Delhi", phone="123456789", course='Java')
# # db.add(s4)
# # s5 = Student(name="Ankit", address="Pune", phone="123456789", course='C++')
# # db.add(s5)
from sqlalchemy import create_engine
from sqlalchemy.sql import text

eng = create_engine("sqlite:///abc.db")

with eng.connect() as con:
    # con.execute(text('DROP TABLE IF EXISTS Cars'))
    # con.execute(text('''CREATE TABLE Cars(Id INTEGER PRIMARY KEY,
    #              Name TEXT, Price INTEGER)'''))
    #
    # data = ({"Id": 1, "Name": "Audi", "Price": 52642},
    #         {"Id": 2, "Name": "Mercedes", "Price": 57127},
    #         {"Id": 3, "Name": "Skoda", "Price": 9000},
    #         {"Id": 4, "Name": "Volvo", "Price": 29000},
    #         {"Id": 5, "Name": "Bentley", "Price": 350000},
    #         {"Id": 6, "Name": "Citroen", "Price": 21000},
    #         {"Id": 7, "Name": "Hummer", "Price": 41400},
    #         {"Id": 8, "Name": "Volkswagen", "Price": 21600}
    #         )
    #
    # for line in data:
    #     con.execute(text("""INSERT INTO Cars(Id, Name, Price)
    #         VALUES(:Id, :Name, :Price)"""), **line)
    #
    #
    #

    rs = con.execute(text('Select * from Cars'))
    print rs.keys()
