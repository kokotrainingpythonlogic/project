from flask import Flask, request, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer, String

app = Flask(__name__)  # type: Flask
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///webTest1.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(Integer, primary_key=True)
    name = db.Column(String)
    address = db.Column(String)

    def __repr__(self):
        return '{} {} {}'.format(self.id, self.name, self.address)


db.create_all()


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('page.html')


@app.route('/createUser', methods=['GET', 'POST'])
def createUser():
    name = request.form['name']
    address = request.form['address']
    u1 = User(name=name, address=address)
    db.session.add(u1)
    db.session.commit()
    return ""


@app.route('/user/<int:id>/')
def user(id):
    return User.query.get_or_404(id).greet()


@app.route('/user/<int:id>/preview/', methods=['POST'])
def preview(id):
    user = User.query.get_or_404(id)
    user.greeting = request.form['name']
    # Expunge `user` from the session so that we can
    # call `db.session.commit` later and do not change
    # user data in table
    db.session.expunge(user)
    return user.greet()


if __name__ == "__main__":
    app.run(debug=True)
