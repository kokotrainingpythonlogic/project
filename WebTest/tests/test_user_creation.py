import pytest
from hamcrest import assert_that, is_

from first import app, db
from flask_webtest import TestApp


class TestUser(object):

    @pytest.fixture(autouse=True)
    def setup(self):
        self.app = app
        self.db = db
        self.test_app = TestApp(self.app, db=self.db, use_session_scopes=True)

    def test1(self):
        response = self.test_app.get("/")
        assert_that(response.status_code, is_(200))

