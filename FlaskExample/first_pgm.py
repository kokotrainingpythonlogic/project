from flask import Flask, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer, String
from werkzeug.utils import redirect

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testMyDb1.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Comment(db.Model):
    __tablename__ = 'comment'
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String)
    comment = db.Column(String)

    def __repr__(self):
        return '{} {} {}'.format(Comment.id, Comment.name, Comment.comment)


@app.route("/")
def index():
    result = Comment.query.all()
    return render_template('index.html', result=result)


@app.route('/deleteUser', methods=['GET', 'POST'])
def deleteUser():
    # name = request.form['name']
    return render_template('deleteUser.html')


@app.route('/delprocess', methods=['GET', 'POST'])
def delprocess():
    name = request.form['name']
    res=db.session.query(Comment).filter_by(name=name).first()
    db.session.delete(res)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/sign')
def sign():
    return render_template('sign.html')


@app.route('/process', methods=['POST'])
def process():
    name = request.form['name']
    comment = request.form['comment']
    signature = Comment(name=name, comment=comment)
    db.session.add(signature)
    db.session.commit()
    return redirect(url_for('index'))
    # result = Comment.query.all()
    # return render_template('index.html', result=result)
    # # return 'Name is : '+name+' and Comment is: '+comment

@app.route('/itemD',methods=['GET','POST'])
def itemD():
    name=req
    return "ABCD"
@app.route("/home", methods=['GET', 'POST'], )
def printName():
    links = ['https://www.google.co.in', 'https://www.facebook.com', 'https://www.gmail.com']
    return render_template('example.html', myvar="Akshay", links=links)  # Remove myVar and Check


if __name__ == "__main__":
    app.run(debug=True)
