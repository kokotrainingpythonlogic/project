from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer, String

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testMyDb.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(Integer, primary_key=True)
    username = db.Column(String)
    email = db.Column(String)

    def __repr__(self):
        return '{} {} {}'.format(User.id, User.username, User.email)
